import paho.mqtt.publish
import wiringpi
import time

btn_pin = 4
wiringpi.wiringPiSetupGpio()
print("Start")
wiringpi.pinMode(btn_pin, wiringpi.OUTPUT)
wiringpi.digitalWrite(btn_pin, wiringpi.LOW)
wiringpi.pinMode(btn_pin, wiringpi.INPUT)
while True:
	zmienna = wiringpi.digitalRead(btn_pin)
	if zmienna:
		paho.mqtt.publish.single(
			topic='button',
			payload='button_off',
			hostname='192.168.1.107',
			port=1883
		)
	else:
		paho.mqtt.publish.single(
			topic='button',
			payload='button_on',
			hostname='192.168.1.107',
			port=1883
		)
	time.sleep(1)

print("Koniec")
