import cv2
import paho.mqtt.client
import paho.mqtt.publish
import io
import time


# obiekt do przechwytywania kamery
cap = cv2.VideoCapture(0)

# teraz cyklicznie
while True:
    ret, frame = cap.read()
    filename = '/SHS/wwwroot/camCapture.png'
    data = cv2.imencode(".png", frame)
    buff = io.BytesIO(data[1])
    with open(filename, 'wb') as f:
        f.write(buff.read())
    time.sleep(0.2)

cap.release()
cv2.destroyAllWindows()
