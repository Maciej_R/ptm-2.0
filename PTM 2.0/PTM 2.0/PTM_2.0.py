# import wiringpi
# import time
import numpy as np
import cv2

print("Start")

#####################################################
###   LAB 1   #######################################
#####################################################

# Zadanie 1
# wiringpi.wiringPiSetupGpio()



# Zadanie 2
def Lab1_zad2():
    wiringpi.pinMode(4, wiringpi.OUTPUT)

    for i in range(15):
        if i%2:
            wiringpi.digitalWrite(4, 0)
            print("Mam nowy")
        else:
            wiringpi.digitalWrite(4, 1)
        time.sleep(1)



# Zadanie 3
def Lab1_zad3():
    wiringpi.pinMode(4, wiringpi.OUTPUT)
    wiringpi.digitalWrite(4, wiringpi.LOW)
    wiringpi.pinMode(4, wiringpi.INPUT)
    for i in range(15):
        zmienna = wiringpi.digitalRead(4)
        print(zmienna)
        time.sleep(1)



# Zadanie 4
def Lab1_zad4():
    counter = 0
    flag = 0

    def gpio_callback():
        global counter, flag
        if flag == 0:
            flag = 1
            print(counter, " GPIO_CALLBACK!")
            counter+=1
            wiringpi.delayMicroseconds(20000)
            flag = 0


    wiringpi.pinMode(4, wiringpi.INPUT)
    wiringpi.pullUpDnControl(4, wiringpi.GPIO.PUD_UP)
    wiringpi.wiringPiISR(4, wiringpi.GPIO.INT_EDGE_FALLING, gpio_callback)

    while True:
        wiringpi.delay(2000)










#####################################################
###   LAB 2   #######################################
#####################################################

# Zad i2c
def twos_complement_combine(msb: int, lsb: int) -> int:
   twos_comp = 256 * msb + lsb
   if twos_comp >= 32768:
       return twos_comp - 65536
   else:
       return twos_comp

def Lab2_zad1():
    wiringpi.wiringPiSetup()
    pin_base = 69

    file_handle = wiringpi.wiringPiI2CSetup(0x6B)

    print(bin(wiringpi.wiringPiI2CReadReg8(file_handle, 0x20)))
    wiringpi.wiringPiI2CWriteReg8(file_handle, 0x20, 0b1111)
    print(bin(wiringpi.wiringPiI2CReadReg8(file_handle, 0x20)))

    while True:
        print(twos_complement_combine(wiringpi.wiringPiI2CReadReg8(file_handle, 0x29), wiringpi.wiringPiI2CReadReg8(file_handle, 0x28)) / 32768, " ",
              twos_complement_combine(wiringpi.wiringPiI2CReadReg8(file_handle, 0x2B), wiringpi.wiringPiI2CReadReg8(file_handle, 0x2A)) / 32768, " ",
              twos_complement_combine(wiringpi.wiringPiI2CReadReg8(file_handle, 0x2D), wiringpi.wiringPiI2CReadReg8(file_handle, 0x2C)) / 32768)
        time.sleep(1)

    # ALTERNATYWNIE

    # while True:
    #     print(twos_complement_combine(wiringpi.wiringPiI2CReadReg8(file_handle, 0x29), wiringpi.wiringPiI2CReadReg8(file_handle, 0x28)), " ",
    #           twos_complement_combine(wiringpi.wiringPiI2CReadReg8(file_handle, 0x2B), wiringpi.wiringPiI2CReadReg8(file_handle, 0x2A)), " ",
    #           twos_complement_combine(wiringpi.wiringPiI2CReadReg8(file_handle, 0x2D), wiringpi.wiringPiI2CReadReg8(file_handle, 0x2C)))
    #    time.sleep(1)










#####################################################
###   LAB 3   #######################################
#####################################################

def Lab3_zad1():
    # obiekt do przechwytywania obrazów z kamery
    cap = cv2.VideoCapture(0)

    # tutaj pobieramy z kamery pojedyncze klatki i inicjalizujemy zmienne do przechowywania obrazów
    ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    prev = gray.copy()
    # teraz cyklicznie
    while True:
        # pobieramy klatkę
        ret, frame = cap.read()
        # konwertujemy obraz kolorowy do obrazu w skali szarości
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # obliczamy wartość bezwzględną z różnicy klatki poprzedniej i aktualnej
        diff = prev.astype(np.float) - gray.astype(np.float)
        diff = np.abs(diff).astype(np.uint8)
        # jeśli suma elementów w obrazie różnicowym jest odpowiednio wysoka wyświetlamy powiadomienie
        if np.sum(diff) > 2000000:
            print("Movement detected!")
        # odkomentowanie dwóch linii poniżej umożliwi nam podglądanie przechwyconej klatki i obrazu różnicowego
        cv2.imshow('frame', gray)
        cv2.imshow('diff', diff)
        # aktualny obraz staje się obrazem poprzednim
        prev = gray.copy()
        # wyjdź i zakończ program po wciśnięciu 'q'
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    # uwalnianie obiektów, zamykanie okien po wyjściu
    cap.release()
    cv2.destroyAllWindows()

def Lab3_zad2():
    # obiekt do przechwytywania obrazów z kamery
    cap = cv2.VideoCapture(0)
    # wczytywanie wyuczonego detektora twarzy
    face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
    # teraz cyklicznie
    while True:
        # odczyt klatki obrazu z kamery
        ret, frame = cap.read()
        # konwersja do obrazu w skali szarości
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # detekcja twarzy w obrazie
        faces = face_cascade.detectMultiScale(gray, 1.3, 5)
        # rysowanie obwódki wokół wykrytych twarzy
        for (x, y, w, h) in faces:
            # i wyświetlanie komunikatu alarmowego
            print("Person detected!")
            img = cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
            roi_color = frame[y:y + h, x:x + w]
        # podgląd w oknie
        cv2.imshow('img', frame)
        # wyjdź i zakończ program po wciśnięciu 'q'
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    # uwalnianie obiektów, zamykanie okien
    cap.release()
    cv2.destroyAllWindows()

	
#####################################################
###  SOCKET  ########################################
#####################################################

import io
def Lab3_zad3():
    s = socket.socket()  # Create a socket object
    host = socket.gethostname()  # Get local machine name
    #host = "127.0.0.1"
    port = 60000  # Reserve a port for your service.
    try:
        s.connect((host, port))
        s.send("Hello server!".encode('ascii'))
    except ConnectionResetError:
        s.close()
        print("Blad polaczenie z serwerem")
        return

    # obiekt do przechwytywania obraz�w z kamery
    cap = cv2.VideoCapture(0)
    if (not cap.isOpened()):
        print("Blad wczytywania kamery")
        return

    # wczytywanie wyuczonego detektora twarzy
    face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
    # teraz cyklicznie
    while True:
        # odczyt klatki obrazu z kamery
        ret, frame = cap.read()
        # konwersja do obrazu w skali szaro�ci
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # detekcja twarzy w obrazie
        faces = face_cascade.detectMultiScale(gray, 1.3, 5)
        for (x, y, w, h) in faces:
            # wy�wietlanie komunikatu alarmowego
            print("Person detected!")
            # frame -> byte_array
            data = cv2.imencode(".png", frame[1])
            # buffer = io.BytesIO(data[1])
            # with open('local_file1.png', 'wb') as f:
            #     f.write(buffer.read())
            # f.close()
            # buffer = io.BytesIO(data[1])
            # with open('local_file.png', 'wb') as f:
            #     l = buffer.read(1024)
            #     while (l):
            #         f.write(l)
            #         l = buffer.read(1024)
            # f.close()
            buffer = io.BytesIO(data[1])

            #buffer = data[1].tobytes()
            # wys�anie danych do serwera
            try:
                l = buffer.read(1024)
                while (l):
                    s.send(l)
                    l = buffer.read(1024)
            except ConnectionResetError:
                s.close()
                cap.release()
                return
            #time.sleep(1)
            s.close()
            cap.release()
            return
        # wyjd� i zako�cz program po wci�ni�ciu 'q'
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    # uwalnianie obiekt�w, zamykanie okien
    s.close()
    cap.release()
	
import socket
def server():
    port = 60000  # Reserve a port for your service.
    s = socket.socket()  # Create a socket object
    host = socket.gethostname()  # Get local machine name
    s.bind((host, port))  # Bind to the port
    s.listen(5)  # Now wait for client connection.

    print('Server listening....')

    counter = 0
    while True:
        conn, addr = s.accept()  # Establish connection with client.
        print('Got connection from', addr)
        data = conn.recv(1024)
        print('Server received', repr(data))

        while True:
            filename = 'Face_' + str(counter) + '.png'
            with open(filename, 'wb') as f:
                while True:
                    data = conn.recv(1024)
                    if not data:
                        break
                    # write data to a file
                    f.write(data)
            f.close()
            print('Successfully get the file')
            break
            # counter+=1
            # if counter >= 5:
            #     break

        # conn.send('Thank you for connecting'.encode('ascii'))
        conn.close()
        break

#####################################################
###   MAIN   ########################################
#####################################################

if __name__ == "__main__":
    Lab3_zad2()

    print("Koniec")