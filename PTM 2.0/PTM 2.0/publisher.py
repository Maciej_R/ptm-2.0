import ssl
import sys

import paho.mqtt.client
import paho.mqtt.publish
import cv2
import io
import time
import pickle

def on_connect(client, userdata, flags, rc):
	print('connected')

def takePhotoAndSend():
	# obiekt do przechwytywani z kamery

	client = paho.mqtt.client.Client()
	client.connect("192.168.1.107")

	cap = cv2.VideoCapture(0)
	if (not cap.isOpened()):
		print("Blad wczytywania kamery")
		cap.release()
		return

	# wczytywanie wyuczonego detektora twarzy
	face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
	# teraz cyklicznie
	while True:
		# odczyt klatki obrazu z kamery
		ret, frame = cap.read()
		# konwersja do obrazu w skali
		gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		# detekcja twarzy w obrazie
		faces = face_cascade.detectMultiScale(gray, 1.3, 5)
		for (x, y, w, h) in faces:
			# wietlanie komunikatu alarmowego
			print("Person detected!")
			# frame -> byte_array
			ret, data = cv2.imencode(".png", frame)
			client.publish("FotkaPeeL", pickle.dumps(data))
			time.sleep(1)

		#if input('') == "q":
		#	client.disconnect()
		#	cap.release()
		#	break

def main():
	paho.mqtt.publish.single(
		topic='pyton',
		payload='Ni wała nie działa',
		hostname='127.0.0.1',
		port=1883,
		retain=True
	)

if __name__ == '__main__':
	takePhotoAndSend()
	sys.exit(0)