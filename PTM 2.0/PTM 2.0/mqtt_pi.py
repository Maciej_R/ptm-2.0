import paho.mqtt.client
import pickle

# client = paho.mqtt.client.Client()
# client.connect(host="192.168.1.107")
# client.publish(topic="test", payload="Test1")
# client.publish(topic="test", payload="Test2")
# client.publish(topic="test", payload="Test3")
# client.disconnect()

def on_connect(client, userdata, flags, rc):
	print('connected (%s)' % client._client_id)
	client.subscribe(topic='FotkaPeeL')

counter = 0
def on_message(client, userdata, message):
    global counter
    print('Otrzymalem wiadomosc')
    data = pickle.loads(message.payload)
    filename = 'Face_' + str(counter) + '.png'
    with open(filename, 'wb') as f:
        f.write(data)
    f.close()
    print('Successfully get the file')
    counter += 1

print("Start")
client = paho.mqtt.client.Client()
client.on_message = on_message
client.on_connect = on_connect
client.connect(host="192.168.1.107")
print("Logged in")
#client.loop_start()
# while input('') != "q":
#     client.loop()
client.loop_forever()
client.disconnect()
print("Koniec")