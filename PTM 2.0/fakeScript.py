import paho.mqtt.publish
import time
import random

while True:
    value = random.randint(0, 4)
    if value == 0:
        paho.mqtt.publish.single(topic='gyroscope', payload="false", hostname='192.168.56.2')
    elif value == 1:
        paho.mqtt.publish.single(topic='gyroscope', payload="true", hostname='192.168.56.2')
    elif value == 2 or value == 5:
        #stats = [random.randint(0, 1000), random.randint(0, 1500)]
        paho.mqtt.publish.single(topic='stats',	payload="stats", hostname='192.168.56.2')
    elif value == 3:
        paho.mqtt.publish.single(topic='button', payload='button_on', hostname='192.168.56.2')
    elif value == 4:
        paho.mqtt.publish.single(topic='button', payload='button_off', hostname='192.168.56.2')
    print(value)
    time.sleep(1)