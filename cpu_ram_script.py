import psutil
import paho.mqtt.publish
import time


while True:
    stats_cpu = psutil.cpu_percent()
    paho.mqtt.publish.single(topic='stats_cpu', payload=stats_cpu, hostname='192.168.1.107', port=1883)
    stats_ram = psutil.virtual_memory()[2]
    paho.mqtt.publish.single(topic='stats_ram', payload=stats_ram, hostname='192.168.1.107', port=1883)
    time.sleep(1)
