FROM raspbian/stretch:latest

RUN apt-get update && apt-get -y install libraspberrypi-bin apt-transport-https

RUN wget -q https://packages.microsoft.com/config/ubuntu/16.04/packages-microsoft-prod.deb
RUN sudo dpkg -i packages-microsoft-prod.deb

RUN apt-get update && apt-get -y install python3 python-pip python-setuptools python-numpy python-opencv

RUN wget -q http://security.debian.org/debian-security/pool/updates/main/o/openssl/libssl1.0.0_1.0.1t-1+deb8u10_armhf.deb
RUN sudo dpkg -i libssl1.0.0_1.0.1t-1+deb8u10_armhf.deb

RUN pip install wiringpi psutil
RUN pip install paho-mqtt

EXPOSE 5000/tcp
