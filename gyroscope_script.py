import paho.mqtt.client
import paho.mqtt.publish
import wiringpi
import time

wiringpi.wiringPiSetupGpio()
wiringpi.wiringPiSetup()

pin_base = 69
file_handle = wiringpi.wiringPiI2CSetup(0x6B)

wiringpi.wiringPiI2CWriteReg8(file_handle, 0x20, 0b1111)

def twos_complement_combine(msb, lsb):
   twos_comp = 256 * msb + lsb
   if twos_comp >= 32768:
       return twos_comp - 65536
   else:
       return twos_comp

x = twos_complement_combine(wiringpi.wiringPiI2CReadReg8(file_handle, 0x29), wiringpi.wiringPiI2CReadReg8(file_handle, 0x28)) / 32768
y = twos_complement_combine(wiringpi.wiringPiI2CReadReg8(file_handle, 0x2B), wiringpi.wiringPiI2CReadReg8(file_handle, 0x2A)) / 32768
z = twos_complement_combine(wiringpi.wiringPiI2CReadReg8(file_handle, 0x2D), wiringpi.wiringPiI2CReadReg8(file_handle, 0x2C)) / 32768

last_x = x
last_y = y
last_z = z

while True:
    x = twos_complement_combine(wiringpi.wiringPiI2CReadReg8(file_handle, 0x29), wiringpi.wiringPiI2CReadReg8(file_handle, 0x28)) / 32768
    y = twos_complement_combine(wiringpi.wiringPiI2CReadReg8(file_handle, 0x2B), wiringpi.wiringPiI2CReadReg8(file_handle, 0x2A)) / 32768
    z = twos_complement_combine(wiringpi.wiringPiI2CReadReg8(file_handle, 0x2D), wiringpi.wiringPiI2CReadReg8(file_handle, 0x2C)) / 32768
    
    print("X =      ", x, " Y =      ", y, " Z =      ", z)
    print("last_x = ", last_x, " last_y = ", last_y, " last_z = ", last_z)

    if last_x != x or last_y != y or last_z != z:
        last_x = x
        last_y = y
        last_z = z

        paho.mqtt.publish.single(topic='gyroscope', payload="true", 
hostname='192.168.1.107', port=1883)
        print("latawica")
    else:
        paho.mqtt.publish.single(topic='gyroscope', payload="false", 
hostname='192.168.1.107', port=1883)
        print("nielatawica")

    time.sleep(1)
