﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MqttLib;

namespace SimpleHTTPServer
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
			connectToBroker();
		}

		public void connectToBroker()
		{
			string connectionString = "tcp://192.168.1.107:1883";
			string clientID = "server";

			try
			{
				var client = MqttClientFactory.CreateClient(connectionString, clientID);
				client.Connect(true);
				client.PublishArrived += new PublishArrivedDelegate(client_PublishArrived);
				client.Subscribe("button", QoS.BestEfforts);
				client.Subscribe("gyroscope", QoS.BestEfforts);
				client.Subscribe("stats_cpu", QoS.BestEfforts);
				client.Subscribe("stats_ram", QoS.BestEfforts);
			}
			catch (MqttException){}
		}

		bool client_PublishArrived(object sender, PublishArrivedArgs e)
		{
			switch(e.Topic)
			{
				case "button":
				{
					File.WriteAllText("button.txt", e.Payload);
					break;
				}
				case "gyroscope":
				{
					File.WriteAllText("gyroscope.txt", e.Payload);
					break;
				}
				case "stats_cpu":
				{
					File.WriteAllText("stats_cpu.txt", e.Payload);
					break;
				}
                case "stats_ram":
                {
                    File.WriteAllText("stats_ram.txt", e.Payload);
                    break;
                }
			}
			return true;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseHsts();
			}

			app.UseHttpsRedirection();
			app.UseStaticFiles();
			app.UseMvc(routes =>
			{
				routes.MapRoute(
					name: "default",
					template: "{controller=Home}/{action=Index}");
			});
		}
	}
}
