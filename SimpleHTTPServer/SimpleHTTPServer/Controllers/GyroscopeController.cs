using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.IO;

namespace SimpleHTTPServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GyroscopeController : ControllerBase
    {
		[HttpGet]
		public ActionResult<string> Get()
		{
			try
			{
				string path = "gyroscope.txt";
				string tempString = System.IO.File.ReadAllText(path);
				return tempString;
			}
			catch (Exception)
			{
				return StatusCode(500);
			}
		}
	}
}