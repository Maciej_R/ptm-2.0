using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.IO;

namespace SimpleHTTPServer.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class HardwareUsageController : ControllerBase
    {
		[HttpGet]
		public ActionResult<string> GetCPU()
		{
			try
			{
				string path = "stats_cpu.txt";
				string tempString = System.IO.File.ReadAllText(path);
				return tempString;
			}
			catch (Exception)
			{
				return StatusCode(500);
			}
		}

        [HttpGet]
        public ActionResult<string> GetRAM()
        {
            try
            {  
                string path = "stats_ram.txt";
                string tempString = System.IO.File.ReadAllText(path);
                return tempString;
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

	}
}
