﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SimpleHTTPServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TempController : ControllerBase
    {
		[HttpGet]
		public ActionResult<string> Get()
		{
			try
			{
				string path = "/sys/class/thermal/thermal_zone0/temp";
				string tempString = System.IO.File.ReadAllText(path);
				return String.Format("{0}", Int32.Parse(tempString) / 1000);
			}
			catch (Exception)
			{
				return StatusCode(500);
			}
		}
	}
}