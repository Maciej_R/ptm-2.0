﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.IO;

namespace SimpleHTTPServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LEDController : ControllerBase
    {
		[HttpGet]
		public ActionResult Get()
		{
			try
			{
				string scriptPath = @"led_script.py";
				//string pythonPath = @"C:\Program Files (x86)\Microsoft Visual Studio\shared\Python36_64\python.exe";
				string pythonPath = @"/usr/bin/python3";
				runPythonScript(scriptPath, pythonPath, false);
				return Ok();
			}
			catch (Exception)
			{
				//return e.StackTrace;
				return StatusCode(500);
			}
		}
		
		[HttpPost]
		public ActionResult Post([FromBody] Dictionary<string, string> JSON)
		{
			int time = 0;
			string value = JSON["value"];
			if (Int32.TryParse(value, out time))
			{
				if (time > 60) time = 60;
				try
				{
					string scriptPath = @"led_script.py";
					string pythonPath = @"/usr/bin/python";
					runPythonScript(String.Format("{0} {1}", scriptPath, time), pythonPath, false);
					return Ok();
				}
				catch (Exception)
				{
					return StatusCode(500);
				}
			}
			else return BadRequest();
		}

		private string wrapPath(string path)
		{
			string wrappedPath = "";
			if (!path.StartsWith("\"")) wrappedPath = "\"";
			wrappedPath += path;
			if (!path.EndsWith("\"")) wrappedPath += "\"";
			return wrappedPath;
		}

		private void runPythonScript(string scriptPath, string pythonPath, bool printOutput=false)
		{
			//scriptPath = wrapPath(scriptPath);
			//pythonPath = wrapPath(pythonPath);
			Process p = new Process();
			p.StartInfo = new ProcessStartInfo(pythonPath, scriptPath)
			{
				RedirectStandardOutput = true,
				UseShellExecute = false,
				CreateNoWindow = true
			};
			p.Start();

			if (printOutput)
			{
				string output = p.StandardOutput.ReadToEnd();
				p.WaitForExit();
				Console.Write(output);
			}
		}
	}
}

// nie wiem, czy powyższe działa ale ja znalazłem takie rozwiązanie - może się przyda:
// https://stackoverflow.com/questions/11779143/how-do-i-run-a-python-script-from-c
