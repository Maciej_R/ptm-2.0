
// odświeżanie informacji

var url = document.location.origin;

function refresh_temp() {
    $.ajax({
        url: url + '/api/temp',
        type: "GET",
        xhrFields: {
            withCredentials: true
        },
        success: function (data) {
            $("#temp").html(data + ' stopni Celsjusza');
        },
        error: function (error) {
            $("#temp").html('-');
        }
    })
}

function refresh_cpu() {
    $.ajax({
        url: url + '/api/hardwareusage/getcpu',
        type: "GET",
        success: function (data) {
            $("#cpu").html(data + ' %');
        },
        error: function (error) {
            $("#cpu").html('-');
        }
    })
}

function refresh_ram() {
    $.ajax({
        url: url + '/api/hardwareusage/getram',
        type: "GET",
        success: function (data) {
            $("#ram").html(data + ' %');
        },
        error: function (error) {
            $("#ram").html('-');
        }
    })
}

function refresh_btn() {
    $.ajax({
        url: url + '/api/button',
        type: "GET",
        success: function (data) {
            if (data == "button_on") {
                turn_on_switch();
            }
            else {
                turn_off_switch();
            }
        },
        error: function (error) {
            turn_off_switch();
        }
    })
}

function refresh_gyro() {
    $.ajax({
        url: url + '/api/gyroscope',
        type: "GET",
        success: function (data) {
            if (data == "true") {
                $('#gyro').html('Płytka porusza się');
                $('#gyro').removeClass('active');
                $('#gyro').addClass('inactive');
            }
            else {
                $('#gyro').html('Płytka jest w spoczynku');
                $('#gyro').removeClass('inactive');
                $('#gyro').addClass('active');
            }
        },
        error: function (error) {
            $('#gyro').html('--brak połączenia--');
            $('#gyro').removeClass('active');
            $('#gyro').addClass('inactive');
        }
    })
}

function update_date() {
    $('#date').html(new Date().toLocaleString("pl-PL"));
}

function refresh_all() {
    refresh_temp();
    refresh_cpu();
    refresh_ram();
    refresh_btn();
    refresh_gyro();
    update_date();
    setTimeout(refresh_all, 500);
}

var refreshing_info = setTimeout(refresh_all, 500);

function stop_refreshing_info() {
    clearTimeout(refreshing_info);
}

// mruganie diodą

function blink_led() {
    var payload = JSON.stringify({ "value": "2" });
    $.ajax({
        url: url + '/api/led',
        type: "POST",
        contentType: 'application/json',
        data: payload
        //success: function (data) {
        //    // do nothing
        //}
    })
}

// reakcja na przycisk

var switch_on = false;
function turn_on_switch() {
    $('#switch').bootstrapToggle('enable');
    $('#switch').bootstrapToggle('on');
    $('#switch-text').html('aktywny');
    $('#switch-text').removeClass('inactive');
    $('#switch-text').addClass('active');
    $('#switch').bootstrapToggle('disable');
    switch_on = true;
}

function turn_off_switch() {
    $('#switch').bootstrapToggle('enable');
    $('#switch').bootstrapToggle('off');
    $('#switch-text').html('nieaktywny');
    $('#switch-text').removeClass('active');
    $('#switch-text').addClass('inactive');
    $('#switch').bootstrapToggle('disable');
    switch_on = false;
}

function toggle_switch() {
    if (switch_on == true) {
        turn_off_switch();
    } else {
        turn_on_switch();
    }
}

// kamerka

function refresh_img() {
    d = new Date();
    img_url = $("#video").attr("src");
    tokens = img_url.split("?");
    $("#video").attr("src", tokens[0] + "?" + d.getTime());
    setTimeout(refresh_img, 200);
}

var refreshing_img = setTimeout(refresh_img, 2000);

function stop_img_refreshing() {
    clearTimeout(refreshing_img);
}