﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace SimpleHTTPServer
{
	public class Program
	{
		public static void Main(string[] args)
		{
			startProcesses();
			CreateWebHostBuilder(args).Build().Run();
			//killProcesses();
		}

		public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
			WebHost.CreateDefaultBuilder(args)
				.UseStartup<Startup>()
				.UseWebRoot("wwwroot")
				.UseUrls("http://0.0.0.0:5000");

		private static Process button, gyroscope, stats, cam;

		private static void startProcesses()
		{
			// TODO: sprawdzenie ścieżek
			string buttonScriptPath = @"btn_script.py";
			string gyroscopeScriptPath = @"gyroscope_script.py";
			string statsScriptPath = @"cpu_ram_script.py";
			string camScriptPath = @"cam_script.py";
			string pythonPath = @"/usr/bin/python";

			try
			{
				button = new Process();
				button.StartInfo = new ProcessStartInfo(pythonPath, buttonScriptPath)
				{
					RedirectStandardOutput = true,
					UseShellExecute = false,
					CreateNoWindow = true
				};
				button.Start();
			}
			catch (Exception) { }

			try
			{
				gyroscope = new Process();
				gyroscope.StartInfo = new ProcessStartInfo(pythonPath, gyroscopeScriptPath)
				{
					RedirectStandardOutput = true,
					UseShellExecute = false,
					CreateNoWindow = true
				};
				gyroscope.Start();
			}
			catch (Exception) { }

			try { 
				stats = new Process();
				stats.StartInfo = new ProcessStartInfo(pythonPath, statsScriptPath)
				{
					RedirectStandardOutput = true,
					UseShellExecute = false,
					CreateNoWindow = true
				};
				stats.Start();
			}
			catch (Exception) { }

			try { 
				cam = new Process();
				cam.StartInfo = new ProcessStartInfo(pythonPath, camScriptPath)
				{
					RedirectStandardOutput = true,
					UseShellExecute = false,
					CreateNoWindow = true
				};
				cam.Start();
			}
			catch (Exception) { }
		}

		private static void killProcesses()
		{
			// Do zatrzymania procesu
			button.Kill();
			gyroscope.Kill();
			stats.Kill();
			cam.Kill();
		}
	}
}
