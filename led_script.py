import wiringpi
import time
import sys

seconds = 2
if len(sys.argv) > 1:
	try:
		seconds = int(sys.argv[1])
	except ValueError:
		seconds = 2
wiringpi.wiringPiSetupGpio()
wiringpi.pinMode(4, wiringpi.OUTPUT)
for i in range(seconds):
	if i%2:
		wiringpi.digitalWrite(4, 0)
	else:
		wiringpi.digitalWrite(4, 1)
	time.sleep(1)
